(function() {
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TopicSchema = new Schema({
  active: Boolean,
  author: String,
  content: String,
  description: String,
  length: String,
  image: String,
  title: String,
  votes: Number,
  voters: [String]
});

module.exports = mongoose.model('Topic', TopicSchema);

})();