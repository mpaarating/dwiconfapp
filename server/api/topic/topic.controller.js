/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /topics              ->  index
 * POST    /topics              ->  create
 * GET     /topics/:id          ->  show
 * PUT     /topics/:id          ->  update
 * DELETE  /topics/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Topic = require('./topic.model');

// Get list of topics
exports.index = function(req, res) {
    Topic.find(function(err, topics) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, topics);
    });
};

// Get a single topic
exports.show = function(req, res) {
    Topic.findById(req.params.id, function(err, topic) {
        if (err) {
            return handleError(res, err);
        }
        if (!topic) {
            return res.send(404);
        }
        return res.json(topic);
    });
};

// Creates a new topic in the DB.
exports.create = function(req, res) {
    Topic.create(req.body, function(err, topic) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, topic);
    });
};

// Updates an existing topic in the DB.
exports.update = function(req, res) {
    // if (req.body._id) {
    //     delete req.body._id;
    // }
    console.log(req);
    Topic.findByIdAndUpdate(req.params.id, req.body, function(err, topic) {
        if (err) {
            return handleError(res, err);
        }
        if (!topic) {
            return res.send(404);
        }
        return res.json(200, topic);
    });
};

// Deletes a topic from the DB.
exports.destroy = function(req, res) {
    Topic.findById(req.params.id, function(err, topic) {
        if (err) {
            return handleError(res, err);
        }
        if (!topic) {
            return res.send(404);
        }
        topic.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    return res.send(500, err);
}
