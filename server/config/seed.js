/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Topic = require('../api/topic/topic.model');
var User = require('../api/user/user.model');

Topic.find({}).remove(function() {
  Topic.create({
    active: true,
    author: 'John Smith',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut, fuga. Nam blanditiis quasi dignissimos non illum necessitatibus voluptatibus eaque corporis, molestiae sed incidunt soluta fugiat suscipit nulla. Totam fuga, ipsam?',
    description : 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.',
    image: 'assets/images/yeoman.png',
    title : 'Development Tools',
    votes: 2
  }, {
    active: false,
    author: 'Jack Collins',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit perspiciatis natus, obcaecati quos provident eos neque nemo tenetur placeat quam explicabo hic dolorum nobis sed impedit, ipsa nostrum nam nesciunt.',
    description : 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.',
    image: 'assets/images/yeoman.png',
    title : 'Server and Client integration',
    votes: 0
  },{
    active: true,
    author: 'Frank Underwood',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit perspiciatis natus, obcaecati quos provident eos neque nemo tenetur placeat quam explicabo hic dolorum nobis sed impedit, ipsa nostrum nam nesciunt.',
    description : 'This is the realest talk about real talk. Come for real talk.',
    image: 'assets/images/yeoman.png',
    title : 'Important Things, Real Talk',
    votes: 8
  }, {
    active: true,
    author: 'Jenny Head',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti sapiente, dolorem, incidunt totam itaque rerum minus asperiores dolore eligendi eveniet dolorum necessitatibus repudiandae animi corporis eos quo aperiam. Neque, quos.',
    description : 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html',
    image: 'assets/images/yeoman.png',
    title : 'Smart Build System',
    votes: 5
  },  {
    active: true,
    author: 'Carrie Shell',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur a odit vero doloribus ipsum quas illum nihil delectus voluptas nemo incidunt est modi excepturi animi, quasi nulla repellat unde. Vero.',
    description : 'Best practice client and server structures allow for more code reusability and maximum scalability',
    image: 'assets/images/yeoman.png',
    title : 'Modular Structure',
    votes: 1
  },  {
    active: true,
    image: 'assets/images/yeoman.png',
    title : 'Optimized Build',
    author: 'Tom Nobland',
    description : 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset titles for caching.',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eos explicabo aspernatur dicta deleniti nam qui beatae, nihil sunt. Nisi dolorem ea iusto nemo ratione, natus vel tempore consectetur ullam?',
    votes: 0
  },{
    active: true,
    author: 'Jim Pokluk',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, quod. Neque optio soluta nostrum unde, nihil numquam autem, velit reiciendis a voluptatibus voluptatem incidunt eligendi odio adipisci minus rerum esse.',
    description : 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators',
    image: 'assets/images/yeoman.png',
    title : 'Deployment Ready',
    votes: 4
  });
});

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test',
    image: 'assets/images/yeoman.png',
    about : {
        twitter: '@man'
    }
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin',
    image: 'assets/images/yeoman.png',
    about : {
        twitter: '@toot'
    }
  }, {
    provider: 'local',
    name: 'Test1',
    email: 'tes1@test.com',
    password: 'test',
    image: 'assets/images/yeoman.png'
  }, {
    provider: 'local',
    name: 'Test2',
    email: 'tes2@test.com',
    password: 'test2',
    image: 'assets/images/yeoman.png'
    },function() {
      console.log('finished populating users');
    }
  );
});
