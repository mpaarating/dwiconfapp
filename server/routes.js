/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var multer = require('multer');
var config = require('./config/environment');

module.exports = function(app) {

  // Insert routes below
  app.use(multer({
    dest: config.root + '/public/assets/profile/img/',
    limits: {
      fieldNameSize: 50,
      files: 1,
      fields: 5,
      fileSize: 1024 * 1024
    },
    rename: function(fieldname, filename) {
      return filename+Date.now();
    },
    onFileUploadStart: function(file) {
      console.log('Starting file upload process.');
      if(file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
        return false;
      }
    },
    inMemory: true //This is important. It's what populates the buffer.
  }));

  app.use('/api/topics', require('./api/topic'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets|uploads)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
