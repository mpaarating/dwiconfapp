'use strict';

/**
 *   Dynamically add alerts to your page.
 *
 *   Usage:
 *       Inject 'AlertSvc' in your controller, add an "<alert-messages></alert-messages>" element to your markup, then call AlertSvc.add() to add a message with the type and text of the message.
 *
 *           AlertSvc.add('info', "DO SOMETHING AAAHHHH!", { timeout: 4500, format: 'alert-thin', icon: 'check-square', title: "Welcome:" });
 *           AlertSvc.add('danger', "HIGH VOLTAGE!");
 *
 *   Parameters:
 *       type: 'info', 'warning', 'danger', or 'success'
 *       text: text of the alert
 *       AlertSvc.noTimeout: OPTIONAL, sets the alert to persist on screen until actively dismissed by the user
 *
 *   Options:
 *       format: follow ui-bootstrap styles for alert, such as 'alert-thin'
 *       timeout: in milliseconds, or AlertSvc.noTimeout
 *       icon: font-awesome icon class, like 'check-square'
 *       title: text to be formatted as "strong", such as "Welcome:" or "Error:"
 *
 *   Usage:
 *       AlertSvc.add('info', "DO SOMETHING AAAHHHH!", { timeout: 4500, format: 'alert-thin', icon: 'check-square', title: "Welcome:" });
 *       AlertSvc.add('danger', "HIGH VOLTAGE!");
 */
angular.module('dwiAlerts', [])
    .factory('AlertSvc', ['$interval', function($interval) {
        return {
            defaultTimeout: 3000,
            alerts: this.alerts || [],
            noTimeout:{noTimeout: 'this indicates there should not be a timeout on this object'},
            setTimeoutOptions: function(options) {
                // The two if clauses allow both AlertSvc('a', 'b', AlertSvc.noTimeout)
                //                               AlertSvc('a', 'b', { 'title': test, timeout: AlertSvc.noTimeout});
                if ((options && options.hasOwnProperty('noTimeout')) || (options && options.timeout === this.noTimeout)) {
                    options.timeout = undefined;
                    return options;
                } else {
                    options = options || {};
                    options.timeout = options.timeout || this.defaultTimeout;
                    options.alertTimesOut = true;
                    return options;
                }
            },
            add: function(type, text, passedOptions) {
                var formats = ['alert-thin', 'format'],
                    types = ['info', 'warning', 'danger', 'success'],
                    service = this,
                    options = this.setTimeoutOptions(passedOptions);

                if (types.indexOf(type) === -1) {
                    throw 'Invalid message type';
                }

                var messageObject = {
                    type: type,
                    text: text,
                    suppress: false,
                    close: function() {
                        return service.remove(this);
                    }
                };

                if (angular.isDefined(options.title)) {
                    messageObject.title = options.title;
                }

                if (angular.isDefined(options.icon)) {
                    messageObject.icon = options.icon;
                } else {
                    if (type === 'success') {
                        messageObject.icon = 'check-square';
                    } else if (type === 'warning' || type === 'danger') {
                        messageObject.icon = 'exclamation-triangle';
                    }
                }

                if (angular.isDefined(options.format)) {
                    if (formats.indexOf(options.format) !== -1) {
                        messageObject.format = options.format;
                    }
                }

                if (angular.isDefined(options.timeout)) {
                    messageObject.timer = $interval(function () {
                        messageObject.close();
                    }, options.timeout, options.alertTimesOut ? 1 : 0 );
                }

                if (angular.isDefined(options.url)) {
                    messageObject.url = options.url;
                }

                if (angular.isDefined(options.code)) {
                    messageObject.code = options.code;
                }

                if (angular.isDefined(options.relay)) {
                    messageObject.relay = options.relay;
                }

                if (angular.isDefined(options.suppress)) {
                    messageObject.suppress = options.suppress;
                }

                var duplicateAlert = false;
                angular.forEach(this.alerts, function(alertObj) {
                    if ((messageObject.type === alertObj.type && messageObject.text === alertObj.text) || (messageObject.code && alertObj.code && messageObject.code === alertObj.code)) {
                       duplicateAlert = true;
                    }
                });

                if (!duplicateAlert && !options.suppress) {
                    this.alerts.push(messageObject);
                    return messageObject;
                }

                return false;
            },
            remove: function (message) {
                var index = this.alerts.indexOf(message);
                this.alerts.splice(index, 1);
            },
            reset: function () {
                this.alerts = [];
            },
            clear: function () {
                angular.forEach(this.alerts, function(alert) {
                    alert.close();
                });
            }
        };
    }])
    .directive('alertMessages', ['$rootScope', 'AlertSvc', function ($rootScope, AlertSvc) {
        return {
            restrict: 'E',
            templateUrl: 'components/alert_message/alert_message.html',
            link: function(scope) {
                scope.alerts = AlertSvc.alerts;
            }
        };
    }]);
