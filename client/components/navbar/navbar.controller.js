(function() {
'use strict';

angular.module('dwiApp')
  .controller('NavbarCtrl', ['$location', 'Auth', function ($location, Auth) {
    this.menu = [{
      'title': 'Home',
      'link': '/'
    }];

    this.isCollapsed = true;
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;

    this.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    this.isActive = function(route) {
      return route === $location.path();
    };
  }]);

})();