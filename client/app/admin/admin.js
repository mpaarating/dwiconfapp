'use strict';

angular.module('dwiApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('admin', {
        url: '/admin',
        templateUrl: 'app/admin/admin.html',
        controller: 'AdminCtrl',
        controllerAs: 'admin',
        data: {
          pageTitle: 'Admin'
        }
      });
  });