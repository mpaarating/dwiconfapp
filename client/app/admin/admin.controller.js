(function() {
    'use strict';

    angular.module('dwiApp')
        .controller('AdminCtrl', ['Auth', 'User', 'TopicSvc', 'AlertSvc', '$log', '$scope', '$timeout', '$state', '$stateParams',
            function(Auth, User, TopicSvc, AlertSvc, $log, $scope, $timeout, $state, $stateParams) {

                this.users = {};
                this.topics = {};
                this.closeOthers = false;
                this.filtered = {
                    active: false
                };
                this.isOpen = [];
                this.filter = '';

                this.userModified = function(user) {
                    user.modified = true;
                };

                this.init = function() {
                    var topicsPromise = TopicSvc.getTopics();
                    var usersPromise = Auth.getAllUsers();

                    var getUsersFn = function(data) {
                        this.users = data;
                        angular.forEach(this.users, function(value, key) {
                            value.modified = false;
                        });

                        // Pagination stuff
                        this.itemsPerPage = 10;
                        this.currentPage = 1;
                        this.totalItems = this.users.length;

                        this.pageCount = function() {
                            return Math.ceil(this.users.length / this.itemsPerPage);
                        }.bind(this);

                        // Checks for page change
                        $scope.$watch('admin.currentPage + admin.itemsPerPage', function() {
                            var begin = ((this.currentPage - 1) * this.itemsPerPage),
                                end = begin + this.itemsPerPage;
                            this.filteredUsers = this.users.slice(begin, end);
                        }.bind(this));

                    }.bind(this);

                    var getTopicsFn = function(resolves) {
                        for (var i = 0; i < resolves.length; i++) {
                            this.isOpen[i] = false;
                        }
                        this.topics = resolves;
                    }.bind(this);

                    var errorHandler = function(errors) {
                        $log.warn(errors);
                    }.bind(this);

                    usersPromise.then(getUsersFn, errorHandler);
                    topicsPromise.then(getTopicsFn, errorHandler);
                }.bind(this);

                this.init();

                this.approveTopic = function(topic) {
                    var topicId = topic._id;
                    topic.active = true;
                    var updatePromise = TopicSvc.updateTopic(topicId, topic);

                    updatePromise.then(
                        function() {
                            AlertSvc.add('success', 'Topic Approved!', {
                                title: 'Success!'
                            });
                        },
                        function() {
                            AlertSvc.add('danger', 'Uh oh! Couldn\'t approve it! Try again', {
                                title: 'ERROR!'
                            });
                        }
                    );
                };

                this.deleteTopic = function(topicId) {
                    var deletePromise = TopicSvc.deleteTopic(topicId);

                    deletePromise.then(
                        function() {
                            this.init();
                            AlertSvc.add('danger', 'Topic Deleted!', {
                                title: 'DELETED!'
                            });
                        }.bind(this),
                        function() {
                            AlertSvc.add('danger', 'Could not delete topic!', {
                                title: 'ERROR!'
                            });
                        }
                    );
                }.bind(this);

                this.updateRole = function(user) {
                    var userId = user._id;
                    var promise = Auth.updateBio(userId, user);
                    promise.then(
                        function() {
                            AlertSvc.add('success', 'Thanks for updating!', {
                                title: 'Success!'
                            });
                        },
                        function() {
                            AlertSvc.add('danger', 'Something went wrong! Try Again!', {
                                title: 'ERROR!'
                            });
                        }
                    );
                };

                this.deleteUser = function(user) {
                    User.remove({
                        id: user._id
                    });
                    AlertSvc.add('danger', 'User deleted!', {
                        title: 'DELETED!'
                    });
                    $state.transitionTo($state.current, $stateParams, {
                        reload: true,
                        inherit: true,
                        notify: true
                    });
                }.bind(this);
            }
        ]);
})();
