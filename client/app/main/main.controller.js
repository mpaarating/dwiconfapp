(function() {
'use strict';

angular.module('dwiApp')
    .controller('MainCtrl', ['TopicSvc', '$log', function(TopicSvc, $log) {
        this.orderProp = '-votes';
        this.limit = 6;
        this.closeOthers = false;
        this.filtered = { active: true };
        this.isOpen = [];

        this.init = function() {
            var topicsPromise = TopicSvc.getTopics();

            var errorHandler = function(errors) {
                $log.warn(errors);
            }.bind(this);

            var getTopicsFn = function(resolves) {
                for (var i = 0; i < resolves.length; i++) {
                  this.isOpen[i] = false;
                }

                this.topics = resolves;
            }.bind(this);

            topicsPromise.then(getTopicsFn, errorHandler);
        };

        this.init();

        this.year = new Date().getFullYear();

    }]);

})();