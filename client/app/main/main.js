(function() {
'use strict';

angular.module('dwiApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        data: {
          pageTitle: 'Dev Workshop Conference'
        }
      });
  });
})();