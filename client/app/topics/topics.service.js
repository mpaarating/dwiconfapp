(function() {
    'use strict';

    /**
     * dwiTopicSvc Module
     *
     * Description
     *  Provides api routes to the backend for all $http methods
     */
    angular.module('dwiTopicSvc', [])
        .factory('TopicSvc', ['$http', '$q',
            function($http, $q) {
                return {

                    simpleGet: function(route) {
                        var deferred = $q.defer();
                        $http.get('/api/' + route)
                            .success(function(data) {
                                deferred.resolve(data);
                            })
                            .error(function(data, status) {
                                status = parseInt(status);
                                var response = {
                                    data: data,
                                    status: status
                                };
                                deferred.reject(response);
                            });
                        return deferred.promise;
                    },

                    getTopics: function() {
                        var url = 'topics';
                        return this.simpleGet(url);
                    },

                    getTopicDetail: function(topicId) {
                        if (topicId === undefined) {
                            var deferred = $q.defer();
                            deferred.reject('Topic ID not provided');
                            return deferred.promise;
                        }

                        var url = 'topics/' + topicId;
                        return this.simpleGet(url);
                    },

                    submitTopic: function(topic) {
                        var deferred = $q.defer();
                        $http.post('/api/topics/', topic)
                            .success(function(data) {
                                deferred.resolve(data);
                            })
                            .error(function(msg) {
                                deferred.reject(msg);
                            });
                        return deferred.promise;
                    },

                    updateTopic: function(topicId, topic) {
                        var deferred = $q.defer();
                        $http.put('/api/topics/' + topicId, topic)
                            .success(function(data) {
                                deferred.resolve(data);
                            })
                            .error(function(msg) {
                                deferred.reject(msg);
                            });
                        return deferred.promise;
                    },

                    deleteTopic: function(topicId) {
                        var deferred = $q.defer();
                        $http.delete('api/topics/' + topicId)
                            .success(function(data) {
                                deferred.resolve(data);
                            })
                            .error(function(msg) {
                                deferred.reject(msg);
                            });
                        return deferred.promise;
                    }

                };
            }
        ]);

})();
