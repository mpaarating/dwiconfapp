(function() {
'use strict';

angular.module('dwiApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('topics', {
        url: '/topics',
        templateUrl: 'app/topics/topics.html',
        controller: 'topicsCtrl',
        controllerAs: 'topics',
        data: {
          pageTitle: 'Topics'
        }
      })
      .state('topic', {
        url: '/topic/:id',
        templateUrl: 'app/topics/topic/topic.html',
        controller: 'topicDetailCtrl',
        controllerAs: 'topic',
        data: {
          pageTitle: 'Topic Detail'
        }
      })
      .state('add-topic', {
        url: '/submit-topic',
        templateUrl: 'app/topics/add-topic/add_topic.html',
        controller: 'addTopicCtrl',
        controllerAs: 'addTopic',
        authenticate: true,
        data: {
          pageTitle: 'Add Topic'
        }
      });
  }]);
})();