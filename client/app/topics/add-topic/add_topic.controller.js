(function() {
'use strict';

angular.module('dwiApp')
    .controller('addTopicCtrl', ['TopicSvc', 'Auth', 'AlertSvc',
    	function(TopicSvc, Auth, AlertSvc) {

        // Grabs user object to associate the saved data with a user
        this.user = Auth.getCurrentUser();
        this.formData = {};
        this.saving = false;
	    	this.topic = {};


	        this.submit = function() {
	        	var formData = this.formData;
	        	var user = this.user;
	            this.saving = true;

	        	var completeData = function(formData, user) {
		        	this.topic.title = formData.title;
		        	this.topic.description = formData.description;
		        	this.topic.content = formData.content;
		        	this.topic.length = formData.length
		        	this.topic.image = user.image;
		        	this.topic.author = user.name;
		        	this.topic.active = false;
		        	this.topic.votes = 0;
	        	}.bind(this);

	        	var resetForm = function() {
		        	this.topic = {};
		        	this.formData = {};
		        	this.saving = false;
		        }.bind(this);

	        	completeData(formData, user);

	            var promise = TopicSvc.submitTopic(this.topic);
	            promise.then(
	                function() {
                    AlertSvc.add('success', 'Thanks for submitting a topic! Your topic is being reviewed!', {title: 'Success!'});
	                    resetForm();
	                },
	                function() {
                    AlertSvc.add('danger', 'Something went wrong! Try Again!', {title: 'ERROR!'});
	                    this.saving = false;
	                }
	            );
	        };

    }]);

})();
