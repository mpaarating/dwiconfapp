(function() {
'use strict';

angular.module('dwiApp')
    .controller('topicDetailCtrl', ['$state', '$log', 'TopicSvc', 'AlertSvc', 'Auth',
        function($state, $log, TopicSvc, AlertSvc, Auth) {
	        var topicId = $state.params.id;
	        var userId = Auth.getCurrentUser()._id;
	        this.detail = {};
	        this.isLoggedIn = Auth.isLoggedIn;
	        this.disabled = false;

	        var voterCheck = function(topic) {
       			if(topic.voters !== undefined){
	       			for (var i = 0; i < topic.voters.length; i++) {
		        		if (topic.voters[i] === userId) {
		        			this.disabled = true;
		        		}
	        		}
       			}
       		}.bind(this);

	        this.init = function() {
	            var topicsPromise = TopicSvc.getTopicDetail(topicId);


	            var errorHandler = function(errors) {
	                $log.warn(errors);
	            };

	            var getTopicFn = function(data) {
	               this.detail = data;
	               voterCheck(this.detail);
	            }.bind(this);

	            topicsPromise.then(getTopicFn, errorHandler);
	        };

	        this.init();

	        this.vote = function(topic) {
	        	var topicId = topic._id;
	        	topic.votes++;
	        	topic.voters.push(userId);
	        	var updatePromise = TopicSvc.updateTopic(topicId, topic);
	        	voterCheck(topic);
            updatePromise.then(
              function() {
                AlertSvc.add('success', 'Thanks for voting!', {title: 'Success!'});
              }, function() {
                AlertSvc.add('danger', 'Your vote didn\'t count! Try Again!', {title: 'ERROR!'});
              }
            );
	        };
    }]);

})();
