(function() {
'use strict';

angular.module('dwiApp')
    .controller('topicsCtrl', ['$log', 'TopicSvc',
        function($log, TopicSvc) {
	        this.topics = {};
	        this.orderProp = '-votes';
	        this.filtered = { active:true };

	        this.init = function() {
	            var topicsPromise = TopicSvc.getTopics();

	            var errorHandler = function(errors) {
	                $log.warn(errors);
	            }.bind(this);

	            var getTopicsFn = function(data) {
	                this.topics = data;
	            }.bind(this);

	            topicsPromise.then(getTopicsFn, errorHandler);
	        };
	        
	        this.init();

    }]);

})();