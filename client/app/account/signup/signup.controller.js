(function() {
'use strict';

angular.module('dwiApp')
  .controller('SignupCtrl', function ($scope, Auth, $location, $window) {
    this.user = {};
    this.errors = {};

    this.register = function(form) {
      this.submitted = true;

      if(form.$valid) {
        Auth.createUser({
          name: this.user.name,
          email: this.user.email,
          password: this.user.password
        })
        .then( function() {
          // Account created, redirect to home
          $location.path('/');
        })
        .catch( function(err) {
          err = err.data;
          this.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            this.errors[field] = error.message;
          });
        });
      }
    };

    this.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
})();