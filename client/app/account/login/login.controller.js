(function() {
'use strict';

angular.module('dwiApp')
  .controller('LoginCtrl', ['Auth', '$location', '$window', function (Auth, $location, $window) {
    this.user = {};
    this.errors = {};

    this.login = function(form) {
      this.submitted = true;

      if(form.$valid) {
        Auth.login({
          email: this.user.email,
          password: this.user.password
        })
        .then( function() {
          // Logged in, redirect to home
          $location.path('/');
        })
        .catch( function(err) {
          this.errors.other = err.message;
        });
      }
    };

    this.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  }]);

})();