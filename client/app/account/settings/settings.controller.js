/// <reference path="../../../../typings/angularjs/angular.d.ts"/>
'use strict';

angular.module('dwiApp')
  .directive('fileModel', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
          scope.$apply(function(){
            modelSetter(scope, element[0].files[0]);
          });
        });
      }
    };
  }])
  .service('fileUpload', ['$http', 'AlertSvc', function ($http, AlertSvc) {
    this.uploadFileToUrl = function(file, uploadUrl){
      var fd = new FormData();
      fd.append('file', file);

      $http.put(uploadUrl, fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
      .success(function(data){
        AlertSvc.add('success', 'Image uploaded!', {title: 'SUCCESS!'});
        return data;
      })
      .error(function(msg){
        AlertSvc.add('danger', 'Upload error!', {title: 'ERROR!'});
        console.log(msg);
      });
    };
  }])
  .controller('SettingsCtrl', ['$scope', 'User', 'Auth', 'fileUpload', 'AlertSvc', '$timeout', '$window', '$http',
    function ($scope, User, Auth, fileUpload, AlertSvc, $timeout, $window, $http) {
      var user = Auth.getCurrentUser();
      var userId = user._id;
      $scope.errors = {};

      $scope.user = user;

      // $scope.uploadFile = function(){
      //   var file = $scope.myFile;
      //   console.log(userId);
      //   var uploadUrl = '/api/users/' + userId + '/image';
      //   fileUpload.uploadFileToUrl(file, uploadUrl);
      //  $timeout(function(){
      //    $window.location.reload();
      //  }, 3000);
      // };

     $scope.saveBio = function(form) {
        user = $scope.user;
        var promise = Auth.updateBio(userId, user);
        promise.then(
            function() {
              AlertSvc.add('success', 'Thanks for updating!', {title: 'Success!'});
            },
            function() {
              AlertSvc.add('danger', 'Something went wrong! Try Again!', {title: 'ERROR!'});
            }
        );
     };

      $scope.changePassword = function(form) {
        if(form.$valid) {
          Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
          .then( function() {
            AlertSvc.add('success', 'Password changed!', {title: 'SUCCESS!'});
              $timeout(function(){
                $window.location.reload();
              }, 2000);
          })
          .catch( function() {
            form.password.$setValidity('mongoose', false);
            $scope.errors.other = 'Incorrect password';
          });
        }
      };
  }]);
