(function() {
    'use strict';

    angular.module('dwiApp', [
            'ngCookies',
            'ngResource',
            'ngSanitize',
            'ui.router',
            'ui.bootstrap',
            'dwiTopicSvc',
            'dwiAlerts',
            'bootstrap.fileField'
        ])
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
            $urlRouterProvider
                .otherwise('/');

            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push('authInterceptor');
        }])

    .factory('authInterceptor', ['$rootScope', '$q', '$cookieStore', '$location',
        function($rootScope, $q, $cookieStore, $location) {
            return {
                // Add authorization token to headers
                request: function(config) {
                    config.headers = config.headers || {};
                    if ($cookieStore.get('token')) {
                        config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
                    }
                    return config;
                },

                // Intercept 401s and redirect you to login
                responseError: function(response) {
                    if (response.status === 401) {
                        $location.path('/login');
                        // remove any stale tokens
                        $cookieStore.remove('token');
                        return $q.reject(response);
                    } else {
                        return $q.reject(response);
                    }
                }
            };
        }
    ])

    .run(['$rootScope', '$location', 'Auth', function($rootScope, $location, Auth) {
        // Redirect to login if route requires auth and you're not logged in
        $rootScope.$on('$stateChangeStart', function(event, next) {
            Auth.isLoggedInAsync(function(loggedIn) {
                if (next.authenticate && !loggedIn) {
                    $location.path('/login');
                }
            });
        });
    }])

    .directive('title', ['$rootScope', '$timeout',
        function($rootScope, $timeout) {
            return {
                link: function() {
                    var listener = function(event, toState) {
                        $timeout(function() {
                            $rootScope.title = (toState.data && toState.data.pageTitle) ? toState.data.pageTitle : 'Dev Workshop Conf';
                        });
                    };
                    $rootScope.$on('$stateChangeSuccess', listener);
                }
            };
        }
    ]);

})();
